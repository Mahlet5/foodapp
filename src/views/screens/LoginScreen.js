import React from 'react';
import {Text, StyleSheet,KeyboardAvoidingView,TouchableWithoutFeedback,Keyboard, View, TextInput} from 'react-native';
import { Button } from 'react-native-elements';

const LoginScreen = ({navigation}) => {
  return (
    <KeyboardAvoidingView style={style.containerView} behavior="padding">

    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <View style={style.loginScreenContainer}>
        <View style={style.loginFormView}>
        <Text style={style.logoText}>CAFETERIA</Text>
          <TextInput placeholder="Username" placeholderColor="#c4c3cb" style={style.loginFormTextInput} />
          <TextInput placeholder="Password" placeholderColor="#c4c3cb" style={style.loginFormTextInput} secureTextEntry={true}/>
          <Button
              buttonStyle={style.loginButton}
              title="Login"
              onPress={() => navigation.navigate('Home')}
            />
        </View>
      </View>
    </TouchableWithoutFeedback>
    </KeyboardAvoidingView>

  );
};


const style = StyleSheet.create({
  containerView: {
      flex: 1,
    },
    loginScreenContainer: {
      flex: 1,
    },
    logoText: {
      color: "#F9813A",
      fontSize: 40,
      fontWeight: "800",
      marginTop: 150,
      marginBottom: 30,
      textAlign: 'center',
    },
    loginFormView: {
      flex: 1
    },
    loginFormTextInput: {
      height: 43,
      fontSize: 14,
      borderRadius: 5,
      borderWidth: 1,
      borderColor: '#eaeaea',
      backgroundColor: '#fafafa',
      paddingLeft: 10,
      marginLeft: 15,
      marginRight: 15,
      marginTop: 5,
      marginBottom: 5,
    
    },
    loginButton: {
      backgroundColor: '#F9813A',
      borderRadius: 5,
      height: 45,
      marginTop: 10,
    },
    fbLoginButton: {
      height: 45,
      marginTop: 10,
      backgroundColor: 'transparent',
    },
});

export default LoginScreen;
